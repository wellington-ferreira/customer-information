import uvicorn
import src.routes
from src.configs import app
import os

if __name__ == '__main__':
    uvicorn.run(
        "server:app",
        host="0.0.0.0", 
        port=int(os.getenv('PORT')),
        debug = False
    )
