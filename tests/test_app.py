import src.routes
from src.configs import app
from fastapi.testclient import TestClient
from faker import Faker

client = TestClient(app)
faker = Faker()

def test_status():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {'status': 200, 'message': 'server running', 'data': {}}

def test_balance():
    account_number = faker.pyint(min_value = 10000, max_value = 99999)
    response = client.get(f"/balance?account_number={account_number}")
    assert response.status_code == 200
    json = response.json()
    assert list(json.keys()) == ['status','message','data']
    assert list(json['data'].keys()) == ['account_number','balance']
    assert json['data']['account_number'] == account_number
    assert json['data']['balance'] >= 0

def test_error_balance():
    account_name = faker.name()
    response = client.get(f"/balance?account_number={account_name}")
    assert response.status_code != 200

def test_registration_data():
    account_number = faker.pyint(min_value = 10000, max_value = 99999)
    response = client.get(f"/registration_data?account_number={account_number}")
    assert response.status_code == 200
    json = response.json()
    assert list(json.keys()) == ['status','message','data']
    assert list(json['data'].keys()) == [
        'account_number', 'name', 'birth', 'email', 'phone_number', 'job', 'country', 'city', 'street', 'number'
    ]
    assert json['data']['account_number'] == account_number

def test_error_registration_data():
    account_name = faker.name()
    response = client.get(f"/registration_data?account_number={account_name}")
    assert response.status_code != 200