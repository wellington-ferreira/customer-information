pydantic==1.9.0
uvicorn==0.17.6
Faker==13.6.0
fastapi==0.75.2
pytest==6.2.5
requests==2.25.1
coverage==6.3.3