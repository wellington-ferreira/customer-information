from faker import Faker
from typing import TypedDict

class RegistrationResponse(TypedDict):
    account_number: int
    name: str
    birth: str
    email: str
    phone_number: str
    job: str
    country: str
    city: str
    street: str
    number: int

class Registration:
    def __init__(self) -> None:
        self.__fake = Faker()

    def get(self, account_number: int) -> RegistrationResponse:
        return {
            "account_number": account_number,
            "name": self.__fake.name(),
            "birth": self.__fake.date_of_birth(minimum_age = 18).strftime("%Y-%m-%d"),
            "email": self.__fake.free_email(),
            "phone_number": self.__fake.phone_number(),
            "job": self.__fake.job(),
            "country": self.__fake.country(),
            "city": self.__fake.city(),
            "street": self.__fake.street_name(),
            "number": self.__fake.pyint(min_value = 1, max_value = 120)

        }