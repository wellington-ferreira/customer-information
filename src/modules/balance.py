from faker import Faker
from typing import TypedDict

class BalanceResponse(TypedDict):
    account_number:int
    balance: dict

class Balance:
    def __init__(self) -> None:
        self.__fake = Faker()

    def get(self, account_number: int) -> BalanceResponse:
        return {
            "account_number": account_number,
            "balance": self.__fake.pyint(
                min_value = 0, 
                max_value = 80_000*100, 
                step = 1
            )
        }