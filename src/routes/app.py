from fastapi import Response, status as status_code
from pydantic import BaseModel
from src.configs import app
from src.modules import Balance, Registration

class ResponseModel(BaseModel):
    status: int
    message: str
    data: dict

@app.get(
    "/", 
    response_model = ResponseModel,
    description="Rota para verificar o status da aplicação")
def status(response: Response):
    try:
        response.status_code = status_code.HTTP_200_OK
        return {"status":200, "message": "server running", "data": {}}
    except:
        response.status_code = status_code.HTTP_500_INTERNAL_SERVER_ERROR
        return {"status":500, "message": "internal error", "data": {}}

@app.get(
    "/balance", 
    response_model = ResponseModel,
    description="Busca o saldo atual da conta")
def balance(account_number: int, response: Response):
    try:
        data = Balance().get(account_number = account_number)
        response.status_code = status_code.HTTP_200_OK
        return {"status":200, "message": "current account balance", "data": data}
    except:
        response.status_code = status_code.HTTP_500_INTERNAL_SERVER_ERROR
        return {"status":500, "message": "internal error", "data": {}}

@app.get(
    "/registration_data", 
    response_model = ResponseModel, 
    description="Busca as informações cadastrais da conta")
def registration_data(account_number: int, response: Response):
    try:
        data = Registration().get(account_number=account_number)
        response.status_code = status_code.HTTP_200_OK
        return {"status":200, "message": "account registration data", "data": data}
    except:
        response.status_code = status_code.HTTP_500_INTERNAL_SERVER_ERROR
        return {"status":500, "message": "internal error", "data": {}}