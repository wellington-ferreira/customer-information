## DevOps
---

Aplicação desenvolvida para o componente referente a DevOps/MLOps do [MBA de Machine Learning in Production da UFScar](https://iti.ufscar.mba/mlp).

O objetivo da tarefa era configurar um processo de CI/CD no GitLab para um aplicativo de nossa escolha. Visando o melhor entendimento da lógica de integração contínua e entrega contínua propriamente optei pelo desenvolvimento de um aplicativo mock que simula uma aplicação que fornece endpoints para consulta de dados cadastrais de clientes, como saldo, endereço, e-mail, etc. 

O processo é executado via runners compartilhados e realiza os seguintes passos (*stages*):
1. Executa os testes definidos na pasta tests/
2. Verifica a cobertura dos testes realizados, sendo definido a cobertura mínima para prosseguimento como 80%
3. Realiza o deploy da aplicação em ambiente de teste
4. Realiza o deploy da aplicação em ambiente de produção

É necessário configurar as seguintes variáveis de ambiente:
* HEROKU_API_KEY: API Key do Heroku
* HEROKU_APP_TEST: Nome da aplicação criada para teste no Heroku
* HEROKU_APP_PROD: Nome da aplicação criada para produção no Heroku

As aplicações criadas com este processo encontram-se nos seguintes links:
* Teste: https://customer-information-test.herokuapp.com/documentation
* Produção: https://customer-information-prod.herokuapp.com/documentation